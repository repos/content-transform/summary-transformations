/**
 * @module lib/mobile-util
 */

import P from 'bluebird';
import domino from 'domino';

const DEFAULT_MAX_MS_PER_TICK = 100;

export const createDocument = ( html: string ) => {
	function pauseAfter( ms ) {
		const start = Date.now();
		return () => ( Date.now() - start ) >= ms;
	}

	function processIncremental( parser ) {
		return new P( ( res, rej ) => setImmediate( () => {
			try {
				if ( parser.process( pauseAfter( DEFAULT_MAX_MS_PER_TICK ) ) ) {
					res( processIncremental( parser ) );
				} else {
					res( parser.document() );
				}
			} catch ( e ) {
				rej( e );
			}
		} ) );
	}

	// @ts-ignore
	return P.resolve( domino.createIncrementalHTMLParser() )
		.then( ( parser ) => {
			parser.end( html );
			return processIncremental( parser );
		} );
};

export default {};
