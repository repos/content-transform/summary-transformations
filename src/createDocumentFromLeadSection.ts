/**
 * @module lib/createDocumentFromLeadSection
 */

import { createDocument } from './lib/domUtils';

/**
 * @param {!string} html Parsoid HTML for the page
 * @return {!Document} a new DOM Document containing only the lead section
 */
export const createDocumentFromLeadSection = ( html: string ) => {
	const sections = html.split( /<section/i ).filter( ( s ) => /^\s+data-mw-section-id=[^>\s\d]*?0[^>]*?>/i.test( s ) );
	return createDocument( sections[ 0 ] ? sections[ 0 ].replace( /[^>]*?>/, '' ).replace( /<\/section>/i, '' ) : '' );
};
