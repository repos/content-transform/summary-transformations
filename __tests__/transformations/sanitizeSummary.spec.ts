import { sanitizeSummary } from '../../src';
const regex = sanitizeSummary.testing;
const sanitize = sanitizeSummary.sanitize;

describe( 'lib:sanitizeSummary', () => {
	const makeLongString = () => {
		const template = 'abcdefghij';
		let result = '';
		for ( let i = 0; i < 8; i++ ) {
			result += template;
		}
		return result;
	};
	const eightyChars = makeLongString();
	const eightyOneChars = `${eightyChars}a`;

	describe( 'regular expressions', () => {
		it( 'ANY_REGEX matches', () => {
			expect( regex.ANY_REGEX.test( '09aAzZ,.#%() _-' ) ).toBeTruthy();
			expect( regex.ANY_REGEX.test( eightyChars ) ).toBeTruthy();
		} );
		it( 'ANY_REGEX does not match', () => {
			expect( !regex.ANY_REGEX.test( '' ) ).toBeTruthy();
			expect( !regex.ANY_REGEX.test( 'foo:bar' ) ).toBeTruthy();
			expect( !regex.ANY_REGEX.test( 'foo/bar' ) ).toBeTruthy();
			expect( !regex.ANY_REGEX.test( 'foo\\bar' ) ).toBeTruthy();
			expect( !regex.ANY_REGEX.test( eightyOneChars ) ).toBeTruthy();
		} );

		it( 'DECIMAL_REGEX matches', () => {
			expect( regex.DECIMAL_REGEX.test( '1' ) ).toBeTruthy();
			expect( regex.DECIMAL_REGEX.test( '0.1' ) ).toBeTruthy();
			expect( regex.DECIMAL_REGEX.test( '.1' ) ).toBeTruthy();
			expect( regex.DECIMAL_REGEX.test( '1.2' ) ).toBeTruthy();
			expect( regex.DECIMAL_REGEX.test( '-1.2' ) ).toBeTruthy();
		} );
		it( 'DECIMAL_REGEX does not match', () => {
			// not sure about the next one, mainly documenting for now
			// needs a number after .
			expect( !regex.DECIMAL_REGEX.test( '1.' ) ).toBeTruthy();
			// needs a number after .
			expect( !regex.DECIMAL_REGEX.test( '.' ) ).toBeTruthy();
			// no letter allowed
			expect( !regex.DECIMAL_REGEX.test( 'a1' ) ).toBeTruthy();
			// no space allowed
			expect( !regex.DECIMAL_REGEX.test( ' ' ) ).toBeTruthy();
		} );

		it( 'CSS_SIZE_REGEX matches', () => {
			expect( regex.CSS_SIZE_REGEX.test( '1.0cm' ) ).toBeTruthy();
			expect( regex.CSS_SIZE_REGEX.test( '-20px' ) ).toBeTruthy();
			expect( regex.CSS_SIZE_REGEX.test( '30%' ) ).toBeTruthy();
		} );
		it( 'CSS_SIZE_REGEX does not match', () => {
			// must have a unit
			expect( !regex.CSS_SIZE_REGEX.test( '1' ) ).toBeTruthy();
			// no letters
			expect( !regex.CSS_SIZE_REGEX.test( 'a1px' ) ).toBeTruthy();
			// no space
			expect( !regex.CSS_SIZE_REGEX.test( '20px and more' ) ).toBeTruthy();
		} );

		it( 'SINGLE_STRING_REGEX matches', () => {
			expect( regex.SINGLE_STRING_REGEX.test( 'green' ) ).toBeTruthy();
			expect( regex.SINGLE_STRING_REGEX.test( 'foo-bar' ) ).toBeTruthy();
			expect( regex.SINGLE_STRING_REGEX.test( '-20px' ) ).toBeTruthy();
			expect( regex.SINGLE_STRING_REGEX.test( eightyChars ) ).toBeTruthy();
		} );
		it( 'SINGLE_STRING_REGEX does not match', () => {
			// no space allowed
			expect( !regex.SINGLE_STRING_REGEX.test( ' ' ) ).toBeTruthy();
			// no space allowed
			expect( !regex.SINGLE_STRING_REGEX.test( 'green and more' ) ).toBeTruthy();
			// no % allowed
			expect( !regex.SINGLE_STRING_REGEX.test( '30%' ) ).toBeTruthy();
			// no : allowed
			expect( !regex.SINGLE_STRING_REGEX.test( 'foo:' ) ).toBeTruthy();
			// no non-ASCII characters allowed
			expect( !regex.SINGLE_STRING_REGEX.test( 'fö' ) ).toBeTruthy();
			// string too long
			expect( !regex.SINGLE_STRING_REGEX.test( eightyOneChars ) ).toBeTruthy();
		} );

		it( 'HEX_REGEX matches', () => {
			expect( regex.HEX_REGEX.test( '#1ac' ) ).toBeTruthy();
			expect( regex.HEX_REGEX.test( '#1A2b3c' ) ).toBeTruthy();
			expect( regex.HEX_REGEX.test( '#1a2B3c4d' ) ).toBeTruthy();
		} );
		it( 'HEX_REGEX does not match', () => {
			// must start with #
			expect( !regex.HEX_REGEX.test( '1ac' ) ).toBeTruthy();
			// no space allowed
			expect( !regex.HEX_REGEX.test( '#1ac and more' ) ).toBeTruthy();
			// no % allowed
			expect( !regex.HEX_REGEX.test( '#30%' ) ).toBeTruthy();
			// no : allowed
			expect( !regex.HEX_REGEX.test( '#foo:' ) ).toBeTruthy();
		} );

		it( 'RGB_REGEX matches', () => {
			expect( regex.RGB_REGEX.test( 'rgb(34, 12, 64, 0.6)' ) ).toBeTruthy();
			expect( regex.RGB_REGEX.test( 'rgba(34,12,64,.6)' ) ).toBeTruthy();
			expect( regex.RGB_REGEX.test( 'rgb(34 12 64 / 0.6)' ) ).toBeTruthy();
			expect( regex.RGB_REGEX.test( 'rgba(34 12 64 / .6)' ) ).toBeTruthy();
			expect( regex.RGB_REGEX.test( 'rgb(34.0 12 64 / 60%)' ) ).toBeTruthy();
			expect( regex.RGB_REGEX.test( 'rgba(34.6 12 64 / 30%)' ) ).toBeTruthy();
		} );
		it( 'RGB_REGEX does not match', () => {
			// must start with rgb( or rgba(
			expect( !regex.RGB_REGEX.test( 'foo' ) ).toBeTruthy();
			// rgb/a needs some arguments
			expect( !regex.RGB_REGEX.test( 'rgb()' ) ).toBeTruthy();
			// must end with )
			expect( !regex.RGB_REGEX.test( 'rgb(134, 112, 164, 0.611) foo' ) ).toBeTruthy();
			// no colon inside ()
			expect( !regex.RGB_REGEX.test( 'rgb(foo:)' ) ).toBeTruthy();
		} );

		it( 'HSL_REGEX matches', () => {
			expect( regex.HSL_REGEX.test( 'hsl(270,60%,70%)' ) ).toBeTruthy();
			expect( regex.HSL_REGEX.test( 'hsl(270, 60%, 70%)' ) ).toBeTruthy();
			expect( regex.HSL_REGEX.test( 'hsl(240 100% 50%)' ) ).toBeTruthy();
			expect( regex.HSL_REGEX.test( 'hsl(270deg, 60%, 70%)' ) ).toBeTruthy();
			expect( regex.HSL_REGEX.test( 'hsl(4.71239rad, 60%, 70%)' ) ).toBeTruthy();
			expect( regex.HSL_REGEX.test( 'hsl(.75turn, 60%, 70%)' ) ).toBeTruthy();

			expect( regex.HSL_REGEX.test( 'hsla(240, 100%, 50%, .05)' ) ).toBeTruthy();
			expect( regex.HSL_REGEX.test( 'hsla(240, 100%, 50%, 1)' ) ).toBeTruthy();
			expect( regex.HSL_REGEX.test( 'hsla(240 100% 50% / .05)' ) ).toBeTruthy();
			expect( regex.HSL_REGEX.test( 'hsla(240 100% 50% / 5%)' ) ).toBeTruthy();
		} );
		it( 'HSL_REGEX does not match', () => {
			// must start with hsl( or hsla(
			expect( !regex.HSL_REGEX.test( 'foo' ) ).toBeTruthy();
			// hsl/a needs some arguments
			expect( !regex.RGB_REGEX.test( 'hsl()' ) ).toBeTruthy();
			// must end with )
			expect( !regex.HSL_REGEX.test( 'hsl(34, 12, 64, 0.6) foo' ) ).toBeTruthy();
			// no colon inside ()
			expect( !regex.HSL_REGEX.test( 'hsl(foo:)' ) ).toBeTruthy();
		} );
	} );

	describe( 'via sanitize-html', () => {
		const assertKeepsAsIs = ( input ) => {
			expect( sanitize( input ) ).toBe( input );
		};

		it( 'removes anchor tags but keeps content (not in allowedTags list)', () => {
			expect( sanitize( '<a>foo</a>' ) ).toBe( 'foo' );
		} );
		it( 'removes script tags (in nonTextTags list)', () => {
			expect( sanitize( '<script>foo</script>' ) ).toBe( '' );
		} );
		it( 'keeps blockquote', () => {
			assertKeepsAsIs( '<blockquote>foo</blockquote>' );
		} );
		it( 'but removes blockquote.cite attribute', () => {
			expect( sanitize( '<blockquote cite="bar">foo</blockquote>' ) ).toBe( '<blockquote>foo</blockquote>' );
		} );
		it( 'keeps abbr with .alt .aria-hidden and .class', () => {
			assertKeepsAsIs( '<abbr alt="bar" aria-hidden="true" class="baz qux">foo</abbr>' );
		} );
		it( 'keeps span.style border', () => {
			// 'almost as-is but an extra space gets removed'
			expect(
				sanitize( '<span style="border: thin dashed red">foo</span>' ) ).toBe( '<span style="border:thin dashed red">foo</span>' );
		} );
		it( 'removes audio tags', () => {
			expect( sanitize( '<audio><source></audio>' ) ).toBe( '' );
		} );
		it( 'removes video tags', () => {
			expect( sanitize( '<video><source><track></video>' ) ).toBe( '' );
		} );
		it( 'keeps img.src, .srcset, .width and .height attributes', () => {
			assertKeepsAsIs( '<img src="//upload.wikimedia.org/wikipedia/commons/3/39/Fluorescence_in_calcite.jpg" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/3/39/Fluorescence_in_calcite.jpg/720px-Fluorescence_in_calcite.jpg 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/3/39/Fluorescence_in_calcite.jpg/960px-Fluorescence_in_calcite.jpg 2x" width="971" height="110" />' );
		} );
		it( 'removes disallowed schemes', () => {
			expect( sanitize( '<img src="ftp://foo.jpg" srcset="file://foo.jpg 1.5x" />' ) ).toBe( '<img />' );
		} );
		it( 'removes background url"', () => {
			expect( sanitize( '<span style="background:url(energize&#x0003A)" />' ) ).toBe( '<span></span>' );
			expect( sanitize( '<span style="background:url(foo)" />' ) ).toBe( '<span></span>' );
		} );
	} );
} );
