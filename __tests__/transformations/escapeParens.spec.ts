import { escapeParens } from '../../src/transformations/escapeParens';

describe( 'lib:escape + unescape parentheses', () => {
	describe( 'Latin parentheses escaping', () => {
		const LATIN_PARENS = 'ab(cd)ef';
		const LATIN_PARENS_ESCAPED = 'ab\uf001cd\uf002ef';

		it( 'properly escapes Latin parentheses', () => {
			expect( escapeParens.escape( LATIN_PARENS ) ).toBe( LATIN_PARENS_ESCAPED );
		} );
		it( 'properly unescapes Latin parentheses', () => {
			expect( escapeParens.unescape( LATIN_PARENS_ESCAPED ) ).toBe( LATIN_PARENS );
		} );
	} );

	describe( 'Non-Latin parentheses escaping', () => {
		const NON_LATIN_PARENS = 'ab（cd）ef';
		const NON_LATIN_PARENS_ESCAPED = 'ab\uf003cd\uf004ef';

		it( 'properly escapes non-Latin parentheses', () => {
			expect( escapeParens.escape( NON_LATIN_PARENS ) ).toBe( NON_LATIN_PARENS_ESCAPED );
		} );
		it( 'properly unescapes non-Latin parentheses', () => {
			expect( escapeParens.unescape( NON_LATIN_PARENS_ESCAPED ) ).toBe( NON_LATIN_PARENS );
		} );
	} );
} );
