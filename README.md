# Summary Transformations
This library is an extract from the Page Content Service (PCS) in the mobileapps repository. The transformations in this library are inteded to be used for summary generation only.

## Preparing the HTML for summary generation
First, you need a parsoid compatible HTML document. With that you need to use the `createDocumentFromLeadSection` function to extract the lead section from the document. This is necessary because the summary generation is only interested in the lead section. After that, you need to apply the transformations to the extracted lead section.

## Applying the transformations
In order to replicate the same output as the PCS service, the transformations need to be applied in the following order:

```yaml
# strip unneeded markup
- rmElements:
  - span:empty
- stripGermanIPA
- rmElements:
  - span[data-mw*="target":{"wt":"IPA"]
  - figure-inline[data-mw*="target":{"wt":"IPA"]
  - span[style="display:none"]
  - span[class*=error]
  - span.Z3988
  - link
  - '#coordinates'
  - table.navbox
  - .geo-nondefault
  - .geo-multi-punct
  - .hide-when-compact
  - div.infobox
  - div.magnify
  - sup.mw-ref
  - .noprint
- rmBracketSpans
- rmComments
- rmAttributes:
    a: ['about', 'data-mw', 'typeof']
    a:not([rel~=nofollow],[rel~=mw:ExtLink]): ['rel']
    abbr: ['title']
    b: ['about', 'data-mw', 'typeof']
    blockquote: ['about', 'data-mw', 'typeof']
    br: ['about', 'data-mw', 'typeof']
    cite: ['about', 'data-mw', 'typeof']
    code: ['about', 'data-mw', 'typeof']
    div: ['data-mw', 'typeof']
    figure: ['typeof']
    # FIXME(T266143): 'figure-inline' is being deprecated
    figure-inline: ['about', 'data-file-type', 'data-mw', 'itemscope', 'itemtype', 'lang', 'rel', 'title', 'typeof']
    i: ['about', 'data-mw', 'typeof']
    img: ['about', 'alt', 'resource']
    li: ['about']
    link: ['data-mw', 'typeof']
    ol: ['about', 'data-mw', 'typeof']
    p: ['data-mw', 'typeof']
    span: ['about', 'data-file-type', 'data-mw', 'itemscope', 'itemtype', 'lang', 'rel', 'title', 'typeof']
    style: ['about', 'data-mw']
    sup: ['about', 'data-mw', 'rel', 'typeof']
    table: ['about', 'data-mw', 'typeof']
    ul: ['about', 'data-mw', 'typeof']
- rmMwIdAttributes
- summarize
```